import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;


public class CalendarTools {

	// yyyy-MM-dd HH:mm:ss 代表24小时制
	// yyyy-MM-dd hh:mm:ss 代表12小时制
	
	@SuppressLint("SimpleDateFormat")
	public static Calendar stringToCalendar(String timeFormat, String time) {
		Calendar calendar = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat(timeFormat);
			Date date = format.parse(time);
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return calendar;
	}

	public static Calendar longToCalendar(long milliseconds) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliseconds);
		return calendar;
	}

}
