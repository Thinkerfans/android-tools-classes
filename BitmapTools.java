package com.android.tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;


/** 
 * @version1.0.0
 * @modify 2014.5.9
 * 
 */
public class BitmapTools {
		
	public Bitmap bytes2Bimap(byte[] b) {  
	    if (b.length != 0) {  
	        return BitmapFactory.decodeByteArray(b, 0, b.length);  
	    } else {  
	        return null;  
	    }  
	} 
	
	public static Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {  
	    int w = bitmap.getWidth();  
	    int h = bitmap.getHeight();  
	    Matrix matrix = new Matrix();  
	    float scaleWidth = ((float) width / w);  
	    float scaleHeight = ((float) height / h);  
	    matrix.postScale(scaleWidth, scaleHeight);  
	    Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);  
	    return newbmp;  
	}  
	
	public static Bitmap zoomBitmap(Bitmap bitmap, int percentage) {  
	    int w = bitmap.getWidth();  
	    int h = bitmap.getHeight();  
	    Matrix matrix = new Matrix();  
	    float scaleWidth = ((float) w * percentage);  
	    float scaleHeight = ((float) h * percentage);  
	    matrix.postScale(scaleWidth, scaleHeight);  
	    Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);  
	    return newbmp;  
	}  
	
	public byte[] bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}
	
	public static Bitmap mergeBitmap(int width,int height ,Bitmap[] cellBitmap,Rect[] rect) {
		Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		for(int i =0;i<cellBitmap.length;i++){
			canvas.drawBitmap(cellBitmap[i], null,rect[i] , null);
		}	
		return bitmap;		
	}
	
	public static Bitmap mergeBitmap(int width,int height ,int[] cellDrawable,Rect[] rect,Context contex) {
		Resources res = contex.getResources();
		Bitmap[] cellBitmap = new Bitmap[cellDrawable.length];
		for(int i = 0;i<cellDrawable.length;i++){
			cellBitmap[i] = BitmapFactory.decodeResource(res, cellDrawable[i]);
		}
		return mergeBitmap(width, height, cellBitmap,rect);		
	}

	public static Bitmap mergeBitmap(int width,int height ,Drawable[] cellIcon,Rect[] rect) {
		Bitmap[] cellBitmap = new Bitmap[cellIcon.length];
		for(int i = 0;i<cellIcon.length;i++){
			cellBitmap[i] = drawable2Bitmap(cellIcon[i]); 
		}
		return mergeBitmap(width, height, cellBitmap,rect);		
	}
	
	public static Drawable bitmap2Drawable(Bitmap bitmap,Context contex){ 		
		Resources res = contex.getResources();     
		BitmapDrawable bd = new BitmapDrawable(res,bitmap);
		return bd;
    }  
	
	/** 
     * Drawable 转 bitmap 
     * @param drawable 
     * @return 
     */  
    public static Bitmap drawable2Bitmap(Drawable drawable){  
        if(drawable instanceof BitmapDrawable){  
            return ((BitmapDrawable)drawable).getBitmap() ;  
        }else if(drawable instanceof NinePatchDrawable){  
            Bitmap bitmap = Bitmap  
                    .createBitmap(  
                            drawable.getIntrinsicWidth(),  
                            drawable.getIntrinsicHeight(),  
                            drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888  
                                    : Bitmap.Config.RGB_565);  
            Canvas canvas = new Canvas(bitmap);  
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),  
                    drawable.getIntrinsicHeight());  
            drawable.draw(canvas);  
            return bitmap;  
        }else{  
            return null ;  
        }  
    }  
	

	public static void saveBitmap(Bitmap bitmap,String path) {		
		FileOutputStream out = null ;
		try {		
			File file = new File(path);	
			out = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			out.flush();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out != null){
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
		}		
	}

	public static Bitmap getBitmap(String path) {
		return BitmapFactory.decodeFile(path);
	}
}
