package com.example.testwidget;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

/**<!-- 在SD卡中创建与删除文件权限 -->
 <uses-permission android:name="android.permission.MOUNT_UNMOUNT_FILESYSTEMS"/>
 <!-- 向SD卡写入数据权限 -->
 <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
 */
public class FileTools {

	public static String TAG = "FileTools";

	/**
	 * 在SD卡根目录下创建文件。
	 * 
	 * @param fileName
	 *            文件名
	 * @param suffix
	 *            文件名后缀
	 * @return 创建的文件
	 */
	public static File createFile(String fileName, String suffix) {
		String path = Environment.getExternalStorageDirectory().getPath();
		String fullName = path + File.separator + fileName + suffix;
		return createFile(fullName);
	}

	/**
	 * 在path目录下创建文件，若path目录之前不存在，则先创建此目录再创建文件。
	 * 
	 * @param path
	 *            文件目录
	 * @param fileName
	 *            文件名
	 * @param suffix
	 *            文件名后缀
	 * @return 创建的文件
	 */
	public static File createFile(String path, String fileName, String suffix) {
		String fullName = path + File.separator + fileName + suffix;
		return createFile(fullName);
	}

	/**
	 * 在path目录下创建文件，若path目录之前不存在，则先创建此目录再创建文件。
	 * 
	 * @param filePath
	 *            文件全名，包括目录在里面。
	 * @return 创建的文件
	 */
	public static File createFile(String filePath) {
		if (TextUtils.isEmpty(filePath)) {
			return null;
		}

		int lastDirSeparator = filePath.lastIndexOf(File.separator);
		File dir = new File(filePath.substring(0, lastDirSeparator + 1));
		if (!dir.exists()) {
			dir.mkdirs();
		}

		File file = new File(filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				Log.w(TAG, "create file fail");
				return null;
			}
		}
		return file;
	}

	/**
	 * 删除filePath代表的这个文件，或者此目录。
	 * 
	 * @param filePath
	 *            文件全名，或者目录
	 */
	public static void deleteFile(String filePath) {
		if (TextUtils.isEmpty(filePath)) {
			return;
		}
		File file = new File(filePath);
		deleteFile(file);
	}

	/**
	 * 删除file这个文件，或者此目录,需要权限
	 * 
	 * @param file
	 *            文件，或者目录
	 */
	public static void deleteFile(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				deleteFile(f);
			}
		}
		file.delete();
	}

	public static String readFile(Context ctx, String fileName) {
		FileInputStream fis = null;
		ByteArrayOutputStream baos = null;
		String str = null;
		try {
			fis = ctx.openFileInput(fileName);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream(1024);
			while (true) {
				int len = fis.read(buffer);
				if (len == -1) {
					break;
				}
				baos.write(buffer, 0, len);
			}
			str = baos.toString("utf-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (baos != null) {
				try {
					baos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return str;
	}

	public static boolean writeFile(Context ctx, String fileName, String content) {
		FileOutputStream fos = null;
		try {
			fos = ctx.openFileOutput(fileName, Activity.MODE_PRIVATE);
			fos.write(content.getBytes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public static String readFile(String fileName) {
		FileReader fr = null;
		BufferedReader br = null;
		StringBuffer sb = null;
		try {
			fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			sb = new StringBuffer();
			String s = "";
			while (true) {
				s = br.readLine();
				if (s == null) {
					break;
				}
				sb.append(s);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (fr != null) {
				try {
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	public static boolean writeFile(String fileName, String content) {
		return writeFile(fileName, content, false);
	}

	public static boolean writeFile(String fileName, String content,
			boolean append) {
		FileWriter fw = null;
		try {
			File file = createFile(fileName);
			fw = new FileWriter(file, append);
			fw.write(content);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public static boolean writeFile(String fileName, InputStream in) {
		return writeFile(fileName, in, false);
	}

	public static boolean writeFile(String fileName, InputStream in,
			boolean append) {
		FileOutputStream fos = null;
		try {
			File file = createFile(fileName);
			fos = new FileOutputStream(file, append);
			byte[] buffer = new byte[1024];
			int len = 0;
			while (true) {
				len = in.read(buffer);
				if (len == -1) {
					break;
				}
				fos.write(buffer, 0, len);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

}
