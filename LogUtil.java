package com.android.cheyooh.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import android.util.Log;

public class LogUtil {
	/** SD卡目录 */
	public static String SDCARD_DIR = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	private static final boolean DEBUG = true;

	public static void i(String tag, String msg) {
		if (DEBUG) {
			Log.i(tag, msg);

		}
	}

	public static void e(String tag, String msg) {
		if (DEBUG) {
			Log.e(tag, msg);

		}
	}

	public static void d(String tag, String msg) {
		if (DEBUG) {
			Log.d(tag, msg);

		}
	}

	public static void w(String tag, String msg) {
		if (DEBUG) {
			Log.w(tag, msg);

		}
	}

	public static void v(String tag, String msg) {
		if (DEBUG) {
			Log.v(tag, msg);

		}
	}

	public static boolean writeFile(String fileName, String content) {
		return writeFile(fileName, content, true);
	}

	public static boolean writeFile(String fileName, String content,
			boolean append) {
		FileWriter fw = null;
		try {
			File file = new File(SDCARD_DIR,fileName);
			fw = new FileWriter(file, append);
			fw.write(content);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}
}
